!R1
en
conf t
h R1

ipv6 unicast-routing

int g0/0/0
ipv6 add fe80::1 link-local
ipv6 add 2001:db8:aaaa:a::/64 eui-64
no sh

!Mac address is 0009.7c71.6202
!EUI-64 0009.7c fffe 71.6202
!Zevende bit 0 0 097c fffe 716 6202
!MAC adres binair:
! 0000 0000 0000 1001 0111 1100 1111 1111 1111 1110 0111 0001 0110
! 0000 0010 0000 1001 0111 1100 1111 1111 1111 1110 0111 0001 0110
!Wordt dus 0209.7cff.fe71.6202
!Haalt met sollicited node netwerk prefix op.
!Vb. 2001:db8:aaaa:a:  0209.7cff.fe71.6202

int g0/0/1
ipv6 add fe80::2 link-local
ipv6 add 2001:db8:aaaa:b::1/64
no sh

end
